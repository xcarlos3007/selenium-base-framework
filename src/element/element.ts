import { Driver } from "selenium-webdriver/chrome";
import { FindElement } from ".";
import { Browser } from "../lib";
import { Log4JS } from "../lib/log4js";

export class Element {
    private find: FindElement;

    public constructor(protected browser: Browser, protected log: Log4JS) {
        this.find = new FindElement(browser, log);
    }

    //Baja el scroll a la posición del elemento
    public async Scroll(selector: string |  undefined, type: string | undefined): Promise<any> {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                // console.log('_ele',_ele)
                const driver = this.browser.getDriver();
                await driver.executeScript("arguments[0].scrollIntoView();", _ele);
            } else {
                this.log.loggererror("CLASS ELEMENT - METODO SCROLL - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type)
                throw new Error("CLASS ELEMENT - METODO SCROLL - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        } catch (error) {
            this.log.loggererror("CLASS ELEMENT - METODO SCROLL - THROW:" + error)
            throw new Error("Se genero un error throw Scroll: " + error);
        }
    }

    //Se agrega un método clear.
    public async onClearInput(selector: string |  undefined, type: string | undefined): Promise<any> {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                await _ele.clear();
            } else {
                this.log.loggererror("CLASS ELEMENT - METODO ONCLEARINPUT - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type)
                throw new Error("CLASS ELEMENT - METODO ONCLEARINPUT - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        } catch (error) {
            this.log.loggererror("CLASS ELEMENT - METODO ONCLEARINPUT - THROW:" + error)
            throw new Error("Se genero un error throw ONCLEARINPUT: " + error);
        }
    }
}