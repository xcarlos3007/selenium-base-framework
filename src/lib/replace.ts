import { Action,ReplaceList } from "../exec/accion.interface.v2";
import { Log4JS } from "./log4js";

export function replacaParameter(target: Action, sourceConfig:any , log: Log4JS) : Action{
    const _list = target.parameters?.commonEdits?.replaceList;
    if(_list){
        _list.forEach( (ele:ReplaceList) => {
            if(ele["parent-key"] == "target" && target.target) {
                ele.values.forEach( (v: string) => { 
                    if(ele.source != "config"){
                        const source = otherValues(ele.source,log)
                        log.loggerinfo("SE GENERO EL VALUE: " + source + "\t PARA LA KEY: "+ v)
                        target.target!.selector = target.target!.selector.replace(v,source)  
                    } else {
                        if(sourceConfig[v]){
                            log.loggerinfo("SE ENCONTRO VALOR PARA LA KEY: " + v + "\t VALOR: " + sourceConfig[v])
                            target.target!.selector = target.target!.selector.replace(v,sourceConfig[v])  
                        } else {
                            log.loggererror("NO SE ENCUENTRA LA KEY: " + v + "\t DENTRO DEL ARCHIVO CONFIG")
                        }
                    }
                })
            } else {
                ele.values.forEach( (v: string) => { 
                    if(ele.source != "config"){                        
                        const source = otherValues(ele.source,log)
                        log.loggerinfo("SE GENERO EL VALUE: " + source + "\t PARA LA KEY: "+ v)
                        target.parameters!.valueToInsert = target.parameters!.valueToInsert!.replace(v, source)
                    }else {
                        if(sourceConfig[v]){
                            log.loggerinfo("SE ENCONTRO VALOR PARA LA KEY: " + v + "\t VALOR: " + sourceConfig[v])
                            target.parameters!.valueToInsert = target.parameters!.valueToInsert!.replace(v, sourceConfig[v])
                        } else {
                            log.loggererror("NO SE ENCUENTRA LA KEY: " + v + "\t DENTRO DEL ARCHIVO CONFIG")
                        }
                    } 
                })
            }
        });
    } 
    return target;
}


export function otherValues(keySource: string, log: Log4JS) : string{
    
    switch (keySource) {
        case "date_utc0":
            let _date = new Date().toISOString().substring(0,10).split("-");
            return _date[2] + "/" + _date[1] + "/" + _date[0];
    
        default:
            log.loggererror("NO EXISTE EL METODO A EJECUTAR: "  + keySource)
            return "";
    }

}


// function replaceValuesInParameter(parameter,values,source){
  
//     myutils.printFunction(getFuncName(), arguments, logMode);
//     for (const variableToReplace of values) {
//         let reemplazo=null;
//         if(source==="config"){
//             reemplazo = getDataJson(objetoUser.variablesUsuario, variableToReplace, false);
//         }
//         else if(source==="client"){
//             reemplazo = getVar(variableToReplace);
//         }
//         else if(source==="DOM"){
//             reemplazo = getVarDOM(variableToReplace);
//         }
//         parameter=myutils.replaceAll(parameter, variableToReplace, reemplazo);
//     }
//     return parameter;
// }
// function makeReplacement(propertyAction,replaceList){
//     myutils.printFunction(getFuncName(), arguments, logMode);
//     let propertyActionCopy=myutils.copyObject(propertyAction);
//     for (const replaceItem of replaceList) {
//         if(propertyActionCopy[replaceItem.parameter]){
//             propertyActionCopy[replaceItem.parameter]=replaceValuesInParameter(
//                 propertyActionCopy[replaceItem.parameter],replaceItem.values,
//                 replaceItem.source
//                 );
//         }
//     }
//     return propertyActionCopy;
// }
// function makeReplacements(myAction){
//     myutils.printFunction(getFuncName(), arguments, logMode);
//     let myActionCopy=myutils.copyObject(myAction);
//     for (const property in myActionCopy) {
//         let replaceList=[];
//         let propertyAction=null;
//         if (Object.hasOwnProperty.call(myActionCopy, property)) {
//             propertyAction = myActionCopy[property];
//             replaceList = getDataJson(propertyAction, "commonEdits.replaceList", true);                
//         }
//         if(Array.isArray(replaceList) &&  replaceList.length>0){
//             propertyAction=makeReplacement(propertyAction,replaceList);
//             myActionCopy[property] = propertyAction;
//         }
//     }
//     return myActionCopy;
// }