import { Alert, Key } from "selenium-webdriver";
import { FindElement } from ".";
import { Browser } from "../lib";
import { Log4JS } from "../lib/log4js";

export class Alerts {


    public constructor(protected browser: Browser, protected log: Log4JS) {
    }

    private async goToAlert(): Promise<Alert> {
        try {
            this.log.loggerinfo("SE INGRESÓ AL ALERT")
            return this.browser.getDriver().switchTo().alert();
        } catch (error) {
            this.log.loggererror("CLASS ALERTS - METODO GOTOALERTS - THROW: " + error)
            throw new Error("Se genero un error throw goToAlert: " + error);
        }

    }

    public async alertAcep(): Promise<void> {
        try {
            const alert = await this.goToAlert();
            alert.accept();
            this.log.loggerinfo("SE ACEPTO EL ALERT")
        } catch (error) {
            this.log.loggererror("CLASS ALERTS - METODO ALERTACEP - THROW: " + error)
            throw new Error("Se genero un error throw alertAcep: " + error);
        }
    }

    public async alertDismiss(): Promise<void> {
        try {
            const alert = await this.goToAlert();
            alert.dismiss();
            this.log.loggerinfo("SE DESCARTO EL ALERT")
        } catch (error) {
            this.log.loggererror("CLASS ALERTS - METODO ALERTDISMISS - THROW: " + error)
            throw new Error("Se genero un error throw alertDismiss: " + error);
        }
    }
}