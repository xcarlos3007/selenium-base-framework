"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Log4JS = void 0;
var log4js = require('log4js'); // include log4js
class Log4JS {
    path;
    constructor(path) {
        this.path = path;
        log4js.configure({
            appenders: {
                error: { type: 'file', filename: path + "/logs/error", pattern: 'yyyy-MM-dd.log', backups: 3, compress: true, keepFileExt: true, alwaysIncludePattern: true },
                debug: { type: 'file', filename: path + "/logs/debug", pattern: 'yyyy-MM-dd.log', backups: 3, compress: true, keepFileExt: true, alwaysIncludePattern: true },
                info: { type: 'file', filename: path + "/logs/info", pattern: 'yyyy-MM-dd.log', backups: 3, compress: true, keepFileExt: true, alwaysIncludePattern: true },
                console: { type: 'console' }
            },
            categories: {
                error: { appenders: ['error', 'console'], level: 'error' },
                debug: { appenders: ['debug', 'console'], level: 'debug' },
                info: { appenders: ['info', 'console'], level: 'info' },
                default: { appenders: ['console', "debug"], level: 'trace' },
            }
        });
    }
    loggerinfo(log) {
        const logger = log4js.getLogger("info");
        logger.info(log);
    }
    loggererror(log) {
        const logger = log4js.getLogger("error");
        logger.error(log);
    }
    loggerdebug(log) {
        const logger = log4js.getLogger("debug");
        logger.debug(log);
    }
}
exports.Log4JS = Log4JS;
