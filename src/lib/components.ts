import { WebElementPromise } from 'selenium-webdriver';

export class WebComponent {
  constructor(protected element: WebElementPromise, public selector: string) { }

  public async click() : Promise<any> {
    return this.element.click()
    .then( () => {
      this.element.getDriver().executeScript('arguments[0].click();', this.element);
    })
    .catch((err) => {
      throw err;
    })
  }

  public async isDisplayed() : Promise<any>  {
    try {
      return await this.element.isDisplayed();
    } catch (ex) {
      return false;
    }
  }

  public async getText() : Promise<any>  {
    return await this.element.getText();
  }
}

export class Button extends WebComponent {
  constructor(element: WebElementPromise, selector: string) {
    super(element, selector);
  }

  public async isDisabled() : Promise<any>  {
    try {
      return await this.element.getAttribute('disabled') === 'disabled';
    } catch (ex) {
      return false;
    }
  }
}

export class TextInput extends WebComponent {
  constructor(element: WebElementPromise, selector: string) {
    super(element, selector);
  }

  public type(text: string) : Promise<any> {
    return this.element.sendKeys(text);
  }
}
