import { Alert, By, Key, WebElementPromise } from "selenium-webdriver";
import { FindElement } from ".";
import { Browser } from "../lib";
import { Log4JS } from "../lib/log4js";

export class Iframe {


    public constructor(protected browser: Browser, protected log: Log4JS) {
    }

    public async setDriverIframe(ele: WebElementPromise): Promise<void> {
        try {
            await this.browser.getDriver().switchTo().frame(ele);
        } catch (error) {
            throw new Error("Se genero un error throw metho = setDriverIframe: " + error);
        }
    }

    public async setDriverDefault(): Promise<void> {
        try {
            const size = await this.browser.getDriver().findElements(By.css("iframe"))

            await this.browser.getDriver().switchTo().defaultContent()
        } catch (error) {
            throw new Error("Se genero un error throw metho = setDriverDefault: " + error);
        }
    }
}


