import { Browser } from '../lib';
import { Log4JS } from '../lib/log4js';
import { FindElement } from './find.element';

export class ClickElement {
    private find: FindElement;

    public constructor(protected browser: Browser, protected log: Log4JS) {
        this.find = new FindElement(browser, log);
    }

    public async onClick(selector?: string , type?: string ): Promise<void> {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type)
                await _ele.click();
            } else {
                this.log.loggererror("CLASS CLICKELEMENT - METODO ONCLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type)
                throw new Error("CLASS CLICKELEMENT - METODO ONCLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        } catch (error) {
            this.log.loggererror("CLASS CLICKELEMENT - METODO ONCLICK -  THOROW:" + error)
            throw new Error("CLASS CLICKELEMENT - METODO ONCLICK -  THOROW:" + error);
        }
    }

    //Se mueve el mouse a la posición del elemneto y se da click
    public async onMoveClick(selector?: string , type?: string ): Promise<void> {
        try {
            if (selector && type) {
                const driver = this.browser.getDriver();
                const _ele = await this.find.findElement(selector, type)
                const actions = driver.actions({ bridge: true });
                await actions.mouseMove(_ele).click().perform();
            } else {
                this.log.loggererror("CLASS CLICKELEMENT - METODO ONMOVECLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type)
                throw new Error("CLASS CLICKELEMENT - METODO ONMOVECLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        } catch (error) {
            this.log.loggererror("CLASS CLICKELEMENT - METODO ONMOVECLICK -  THOROW:" + error)
            throw new Error("CLASS CLICKELEMENT - METODO ONMOVECLICK -  THOROW: " + error);
        }
    }

    //Doble Click
    public async onDoubleClick(selector?: string , type?: string ): Promise<void> {
        try {
            if (selector && type) {
                const driver = this.browser.getDriver();
                const _ele = await this.find.findElement(selector, type)
                const actions = driver.actions({ bridge: true });
                await actions.mouseMove(_ele).doubleClick().perform();
            } else {
                this.log.loggererror("CLASS CLICKELEMENT - METODO ONDOUBLECLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type)
                throw new Error("CLASS CLICKELEMENT - METODO ONDOUBLECLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        } catch (error) {
            this.log.loggererror("CLASS CLICKELEMENT - METODO ONDOUBLECLICK -  THOROW:" + error)
            throw new Error("CLASS CLICKELEMENT - METODO ONDOUBLECLICK -  THOROW: " + error);
        }
    }

}