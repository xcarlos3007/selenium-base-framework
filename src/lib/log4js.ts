var log4js = require('log4js'); // include log4js


export class Log4JS {
    public constructor(protected path: string) {
        log4js.configure({ // configure to use all types in different files.
            appenders:
            {
                error: { type: 'file', filename: path + "/logs/error", pattern: 'yyyy-MM-dd.log', backups: 3, compress: true, keepFileExt: true, alwaysIncludePattern: true },
                debug: { type: 'file', filename: path + "/logs/debug", pattern: 'yyyy-MM-dd.log', backups: 3, compress: true, keepFileExt: true, alwaysIncludePattern: true },
                info: { type: 'file', filename: path + "/logs/info", pattern: 'yyyy-MM-dd.log', backups: 3, compress: true, keepFileExt: true, alwaysIncludePattern: true },
                console: { type: 'console' }
            },
            categories: {
                error: { appenders: ['error','console'], level: 'error' },
                debug: { appenders: ['debug', 'console'], level: 'debug' },
                info: { appenders: ['info', 'console'], level: 'info' },
                default: { appenders: ['console', "debug"], level: 'trace' },
            }
        });
    }

    public loggerinfo(log: string) {
        const logger = log4js.getLogger("info");
        logger.info(log);
    }

    public loggererror(log: string): any {
        const logger = log4js.getLogger("error");
        logger.error(log);
    }

    public loggerdebug(log: string): any {
        const logger = log4js.getLogger("debug");
        logger.debug(log);
    }
}

