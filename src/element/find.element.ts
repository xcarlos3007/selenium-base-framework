import { By, WebElement, WebElementPromise } from "selenium-webdriver";
import { Browser } from "../lib";
import { Log4JS } from "../lib/log4js";

export class FindElement {
    
    public constructor(protected browser: Browser,protected log: Log4JS) {
    }

    public  findElement(selector?: string , type?: string ,shadow?: WebElement): WebElementPromise {
        // console.log('type.toUpperCase()',type.toUpperCase())
        try {
            if(selector && type){
                switch (type.toUpperCase()) {
                    case 'ID':
                        return this.findElementID(selector,shadow);
                    case 'CSS':
                        return this.findElementCss(selector,shadow);
                    case 'XPATH':
                        return this.findElementXPATH(selector,shadow);
                    case 'CLASS':
                        return this.findElementClass(selector,shadow);
                    default:
                        return this.findElementID(selector,shadow);
                }
            } else {
                this.log.loggererror("CLASS FINDELEMENT - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" +type)
                throw new Error("CLASS FINDELEMENT - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" +type);
            }
        } catch (error) {
            this.log.loggererror("CLASS FINDELEMENT - NO PUDO ENCONTRAR EL ELEMENTO:" + error)
            throw new Error("Se genero un error throw find Element: " + error);
        }
        
    }

    private  findElementCss(selector: string, shadow: WebElement|undefined): WebElementPromise {
        const driver = this.browser.getDriver() || shadow;
        return  driver.findElement(By.css(selector));
    }
    private  findElementID(selector: string ,shadow: WebElement|undefined): WebElementPromise {
        const driver = this.browser.getDriver() || shadow;
        return driver.findElement(By.id(selector))
    }
    private  findElementXPATH(selector: string,shadow: WebElement|undefined): WebElementPromise {
        const driver = this.browser.getDriver() || shadow;
        // console.log('xpath', driver);
        return driver.findElement(By.xpath(selector))
    }
    private findElementClass(selector: string,shadow: WebElement|undefined): WebElementPromise {
        const driver = this.browser.getDriver() || shadow;
        return driver.findElement(By.className(selector))
    }

}
