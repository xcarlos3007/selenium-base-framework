"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TextInput = exports.Button = exports.WebComponent = void 0;
class WebComponent {
    element;
    selector;
    constructor(element, selector) {
        this.element = element;
        this.selector = selector;
    }
    async click() {
        return this.element.click()
            .then(() => {
            this.element.getDriver().executeScript('arguments[0].click();', this.element);
        })
            .catch((err) => {
            throw err;
        });
    }
    async isDisplayed() {
        try {
            return await this.element.isDisplayed();
        }
        catch (ex) {
            return false;
        }
    }
    async getText() {
        return await this.element.getText();
    }
}
exports.WebComponent = WebComponent;
class Button extends WebComponent {
    constructor(element, selector) {
        super(element, selector);
    }
    async isDisabled() {
        try {
            return await this.element.getAttribute('disabled') === 'disabled';
        }
        catch (ex) {
            return false;
        }
    }
}
exports.Button = Button;
class TextInput extends WebComponent {
    constructor(element, selector) {
        super(element, selector);
    }
    type(text) {
        return this.element.sendKeys(text);
    }
}
exports.TextInput = TextInput;
