"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClickElement = void 0;
const find_element_1 = require("./find.element");
class ClickElement {
    browser;
    log;
    find;
    constructor(browser, log) {
        this.browser = browser;
        this.log = log;
        this.find = new find_element_1.FindElement(browser, log);
    }
    async onClick(selector, type) {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                await _ele.click();
            }
            else {
                this.log.loggererror("CLASS CLICKELEMENT - METODO ONCLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
                throw new Error("CLASS CLICKELEMENT - METODO ONCLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        }
        catch (error) {
            this.log.loggererror("CLASS CLICKELEMENT - METODO ONCLICK -  THOROW:" + error);
            throw new Error("CLASS CLICKELEMENT - METODO ONCLICK -  THOROW:" + error);
        }
    }
    //Se mueve el mouse a la posición del elemneto y se da click
    async onMoveClick(selector, type) {
        try {
            if (selector && type) {
                const driver = this.browser.getDriver();
                const _ele = await this.find.findElement(selector, type);
                const actions = driver.actions({ bridge: true });
                await actions.mouseMove(_ele).click().perform();
            }
            else {
                this.log.loggererror("CLASS CLICKELEMENT - METODO ONMOVECLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
                throw new Error("CLASS CLICKELEMENT - METODO ONMOVECLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        }
        catch (error) {
            this.log.loggererror("CLASS CLICKELEMENT - METODO ONMOVECLICK -  THOROW:" + error);
            throw new Error("CLASS CLICKELEMENT - METODO ONMOVECLICK -  THOROW: " + error);
        }
    }
    //Doble Click
    async onDoubleClick(selector, type) {
        try {
            if (selector && type) {
                const driver = this.browser.getDriver();
                const _ele = await this.find.findElement(selector, type);
                const actions = driver.actions({ bridge: true });
                await actions.mouseMove(_ele).doubleClick().perform();
            }
            else {
                this.log.loggererror("CLASS CLICKELEMENT - METODO ONDOUBLECLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
                throw new Error("CLASS CLICKELEMENT - METODO ONDOUBLECLICK - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        }
        catch (error) {
            this.log.loggererror("CLASS CLICKELEMENT - METODO ONDOUBLECLICK -  THOROW:" + error);
            throw new Error("CLASS CLICKELEMENT - METODO ONDOUBLECLICK -  THOROW: " + error);
        }
    }
}
exports.ClickElement = ClickElement;
