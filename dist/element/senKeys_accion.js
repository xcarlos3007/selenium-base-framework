"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendKeysAccion = void 0;
const selenium_webdriver_1 = require("selenium-webdriver");
const _1 = require(".");
class SendKeysAccion {
    browser;
    log;
    find;
    constructor(browser, log) {
        this.browser = browser;
        this.log = log;
        this.find = new _1.FindElement(browser, log);
    }
    async onEnter(selector, type) {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                await _ele.sendKeys(selenium_webdriver_1.Key.ENTER);
            }
            else {
                this.log.loggererror("CLASS SENDKEYS - METODO ONENTER - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
                throw new Error("CLASS SENDKEYS - METODO ONENTER - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        }
        catch (error) {
            this.log.loggererror("CLASS SENDKEYSACCION - METODO ONENTER - THROW: " + error);
            throw new Error("Se genero un error throw onEnter: " + error);
        }
    }
}
exports.SendKeysAccion = SendKeysAccion;
