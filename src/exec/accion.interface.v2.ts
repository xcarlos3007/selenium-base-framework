import { cutImg } from "../element/screenshot.element.";

export interface Action {
    index?: number,
    action: string,
    target?: target
    parameters?: parameters,
}

export interface target {
    selector: string,
    type: string
}

export interface targetIframe {
    iframes : Array<target>
    exit: boolean
}

interface parameters {
    valueToInsert?: string,
    commonEdits?: commonEdits,
    url?: string,
    "time-sleep-out"?: number,
    saveID?: string,
    nameImage?: string
    iframe?: targetIframe,
    cut?: cutImg,
    timeoutElement? : number,
    time?: number,
    force_error?: forceError
}

interface commonEdits {
    replaceList?: Array<ReplaceList>
}

export interface forceError {
    generate_excepcion: boolean,
    exeIndex: number,
    source: true,
    nameSource: string
}

export interface ReplaceList {
    parameter: string,
    source: string,
    values : Array<string>,
    "parent-key" : string
}
