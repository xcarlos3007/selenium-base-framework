"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.execAction = void 0;
const selenium_webdriver_1 = require("selenium-webdriver");
const element_1 = require("../element");
const element_2 = require("../element/element");
const screenshot_element_1 = require("../element/screenshot.element.");
const senKeys_accion_1 = require("../element/senKeys_accion");
const replace_1 = require("../lib/replace");
const error_element_1 = require("../element/error.element");
// eslint-disable-next-line @typescript-eslint/no-unused-vars
async function execAction(browser, action, driver, page, log, source) {
    const _iframe = new element_1.Iframe(browser, log);
    try {
        const _find = new element_1.FindElement(browser, log);
        let _root, locator;
        ///BUSCAMOS SI LA ACCION TIENE ELEMENTOS A REMPLAZAR
        action = (0, replace_1.replacaParameter)(action, source, log);
        if (action.parameters?.iframe && !!action.parameters?.iframe?.iframes?.length) {
            action.parameters?.iframe.iframes.forEach(async (f) => {
                if (!!f.selector && f.selector != "" || !f.type && f.type != "") {
                    _root = _find.findElement(f.selector, f.type);
                    await _iframe.setDriverIframe(_root);
                }
            });
        }
        if (action.target?.type && action.target?.selector) {
            switch (action.target?.type) {
                case "css":
                    locator = { css: action.target.selector };
                    break;
                case "id":
                    locator = { id: action.target.selector };
                    break;
                case "xpath":
                    locator = { xpath: action.target.selector };
                    break;
                default:
                    locator = { id: action.target.selector };
                    break;
            }
            // console.log('locator',locator)
            if (action.parameters?.timeoutElement) {
                const condition = selenium_webdriver_1.until.elementLocated(locator);
                await driver.wait(condition, action.parameters?.timeoutElement);
            }
        }
        // console.log('accion', action)
        log.loggerinfo("ACCION EJECUTADA: " + JSON.stringify(action, null, 4));
        const rs = await _action(action, browser, log);
        // log.loggerdebug("index:_" +rs)
        if (!!action.parameters?.iframe && action.parameters?.iframe?.exit) {
            await _iframe.setDriverDefault();
            log.loggerinfo("SE SALIO AL IFREME DEFAULT");
            await browser.sleep(3000);
        }
        // log.loggerdebug("paso 1:")
        if (action.parameters?.["time-sleep-out"] && action.action.toUpperCase() != 'SLEEP' && action.action.toUpperCase() != 'WAIT') {
            await browser.sleep(action.parameters?.["time-sleep-out"]);
        }
        else if (action.action.toUpperCase() != 'SLEEP' && action.action.toUpperCase() != 'WAIT') {
            await browser.sleep(3000);
        }
        // log.loggerdebug("index return:_" +rs)
        return rs;
    }
    catch (error) {
        // log.loggerdebug("llego error: " + error)
        if (!!action.parameters?.iframe && action.parameters?.iframe?.exit) {
            await _iframe.setDriverDefault();
            log.loggerinfo("SE SALIO AL IFREME DEFAULT");
            await browser.sleep(3000);
        }
        log.loggererror("ERROR AL EJECUTAR LA ACCION : " + JSON.stringify(action, null, 4));
        throw new Error("ERROR AL EJECUTAR LA ACCION: " + error);
    }
}
exports.execAction = execAction;
async function _action(action, browser, log) {
    const _click = new element_1.ClickElement(browser, log);
    const _find = new element_1.FindElement(browser, log);
    const _elem = new element_2.Element(browser, log);
    const _screen = new screenshot_element_1.ScreenShotElement(browser, log);
    const _sendKeysAction = new senKeys_accion_1.SendKeysAccion(browser, log);
    const _alert = new element_1.Alerts(browser, log);
    const _errorEle = new error_element_1.ErrorElement(browser, log);
    let domShadowRoot;
    switch (action.action.toUpperCase()) {
        case 'FIND':
            await _find.findElement(action.target?.selector, action.target?.type, domShadowRoot);
            break;
        case 'FILL':
            // eslint-disable-next-line no-case-declarations
            const _eFill = await _find.findElement(action.target?.selector, action.target?.type, domShadowRoot);
            await _eFill.sendKeys(action.parameters?.valueToInsert || "");
            break;
        case 'CLICK':
            await _click.onClick(action.target?.selector, action.target?.type);
            break;
        case 'MOVE-CLICK':
            await _click.onMoveClick(action.target?.selector, action.target?.type);
            break;
        case 'WAIT':
            await browser.wait(_find.findElement(action.target?.selector, action.target?.type, domShadowRoot), action.parameters?.time);
            break;
        case 'SLEEP':
            await browser.sleep(action.parameters?.time);
            break;
        case 'SCROLL':
            await _elem.Scroll(action.target?.selector, action.target?.type);
            break;
        case 'SCREEN-FIND-ELEMENT':
            await _screen.ScreenFindEle(action.target?.selector, action.target?.type);
            break;
        case 'COPY-SCREEN':
            await _screen.copyScreen();
            break;
        case 'SCREEN-FIND-ELEMENT-SAVE':
            await _screen.ScreenFindEleSave(action.target?.selector, action.target?.type, action.parameters?.saveID);
            break;
        case 'PASTE-SCREEN':
            await _screen.pasteScreen(action.target?.selector, action.target?.type);
            break;
        case 'UPDATE-URL':
            await browser.getDriver().navigate().to(action.parameters?.url || "");
            break;
        case 'RELOAD':
            await browser.getDriver().navigate().refresh();
            break;
        // case 'GENERATE-IMAGE':
        //     await _screen.generateImage(action.saveID)
        //     break;
        case 'ENTER':
            await _sendKeysAction.onEnter(action.target?.selector, action.target?.type);
            break;
        case 'SCREEN-FIND-GENERATE':
            // eslint-disable-next-line no-case-declarations
            const _imgBs64 = await _screen.ScreenFindEle(action.target?.selector, action.target?.type);
            await _screen.ScreenGenerate(_imgBs64, action.parameters?.nameImage);
            break;
        case 'CUT-IMAGE':
            await _screen.cutImage(action.parameters?.cut);
            break;
        case 'SCREEN-ALL-GENERATE':
            // eslint-disable-next-line no-case-declarations
            const _screenBase64 = await _screen.ScreenAll();
            await _screen.ScreenGenerate(_screenBase64, action.parameters?.nameImage);
            break;
        case 'FIND-FORCE-ERROR':
            await _errorEle.findForceError(action.target?.selector, action.target?.type, action.parameters?.force_error);
            break;
        case 'GO-TO-IFRAME':
            log.loggerinfo(`SE INGRESO AL IFRAME: ${JSON.stringify(action.parameters?.iframe, undefined, 4)}`);
            break;
        case 'EXIT-IFRAME':
            log.loggerinfo(`SE SALIO DEL IFRAME: ${JSON.stringify(action.parameters?.iframe, undefined, 4)}`);
            break;
        case 'ALERT-ACCEPT':
            await _alert.alertAcep();
            break;
        case 'ALERT-DISMISS':
            await _alert.alertDismiss();
            break;
        case 'DOUBLE-CLICK':
            await _click.onMoveClick(action.target?.selector, action.target?.type);
            break;
        case 'CLEAR-INPUT':
            await _elem.onClearInput(action.target?.selector, action.target?.type);
            break;
        default:
            log.loggererror('(NTTDATA) NO EXISTE LA ACCION A REALIZAR:' + action.action.toUpperCase());
            break;
    }
    return;
}
