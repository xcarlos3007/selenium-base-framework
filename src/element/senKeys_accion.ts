import { Key } from "selenium-webdriver";
import { FindElement } from ".";
import { Browser } from "../lib";
import { Log4JS } from "../lib/log4js";

export class SendKeysAccion {
    private find: FindElement;

    public constructor(protected browser: Browser, protected log: Log4JS) {
        this.find = new FindElement(browser, log);
    }

    public async onEnter(selector: string | undefined, type: string | undefined): Promise<void> {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type)
                await _ele.sendKeys(Key.ENTER)
            } else {
                this.log.loggererror("CLASS SENDKEYS - METODO ONENTER - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type)
                throw new Error("CLASS SENDKEYS - METODO ONENTER - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        } catch (error) {
            this.log.loggererror("CLASS SENDKEYSACCION - METODO ONENTER - THROW: " + error)
            throw new Error("Se genero un error throw onEnter: " + error);
        }

    }
}