import { WebElement } from 'selenium-webdriver';
import { Browser, WaitCondition } from './';
import { elementIsPresent } from './conditions';

export interface NewablePage<T extends Page> {
  new(browser: Browser): T;
}

export  class Page {
  private url: string;
  
  public setUrl(url: string): void {
    this.url = url;
  }

  public async navigate(): Promise<void> {
    await this.browser.navigate(this.url);
    // await this.browser.wait(this.loadCondition(ele));
  }
  public  loadCondition(ele:WebElement): WaitCondition {
    return elementIsPresent(() => ele);
  }

  public constructor(protected browser: Browser) {

  }
}
