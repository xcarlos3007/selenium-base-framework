"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.elementIsPresent = exports.elementIsVisible = void 0;
function elementIsVisible(locator) {
    return async () => await locator().isDisplayed();
}
exports.elementIsVisible = elementIsVisible;
function elementIsPresent(locator) {
    return async () => await locator() !== undefined;
}
exports.elementIsPresent = elementIsPresent;
