"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Iframe = void 0;
const selenium_webdriver_1 = require("selenium-webdriver");
class Iframe {
    browser;
    log;
    constructor(browser, log) {
        this.browser = browser;
        this.log = log;
    }
    async setDriverIframe(ele) {
        try {
            await this.browser.getDriver().switchTo().frame(ele);
        }
        catch (error) {
            throw new Error("Se genero un error throw metho = setDriverIframe: " + error);
        }
    }
    async setDriverDefault() {
        try {
            const size = await this.browser.getDriver().findElements(selenium_webdriver_1.By.css("iframe"));
            await this.browser.getDriver().switchTo().defaultContent();
        }
        catch (error) {
            throw new Error("Se genero un error throw metho = setDriverDefault: " + error);
        }
    }
}
exports.Iframe = Iframe;
