"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Browser = void 0;
require("chromedriver");
require("edgedriver");
const selenium_webdriver_1 = require("selenium-webdriver");
class Browser {
    driver;
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    constructor(browserName, server) {
        this.driver = new selenium_webdriver_1.Builder().forBrowser(browserName)
            .usingServer(server)
            .build();
        this.driver.manage().window().maximize();
    }
    async navigate(url) {
        await this.driver.navigate().to(url);
    }
    getDriver() {
        return this.driver;
    }
    async clearCookies(url) {
        if (url) {
            const currentUrl = await this.driver.getCurrentUrl();
            await this.navigate(url);
            await this.driver.manage().deleteAllCookies();
            await this.navigate(currentUrl);
        }
        else {
            await this.driver.manage().deleteAllCookies();
        }
    }
    async wait(locator, time = 5000) {
        await this.driver.wait(locator, time);
    }
    async sleep(time) {
        await this.driver.sleep(time || 5000);
    }
    async waitAny(conditions) {
        const all = (!(conditions instanceof Array)) ? [conditions] : conditions;
        await this.driver.wait(async () => {
            for (const condition of all) {
                try {
                    if (await condition(this) === true) {
                        return true;
                    }
                    continue;
                }
                catch (ex) {
                    continue;
                }
            }
        });
    }
    async close() {
        await this.driver.close();
    }
}
exports.Browser = Browser;
