"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setPath = exports.getPath = exports.initPath = void 0;
let pathFile = '';
function initPath(path) {
    return pathFile = path;
}
exports.initPath = initPath;
function getPath() {
    return pathFile;
}
exports.getPath = getPath;
function setPath(path) {
    return pathFile = path;
}
exports.setPath = setPath;
