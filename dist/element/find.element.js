"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FindElement = void 0;
const selenium_webdriver_1 = require("selenium-webdriver");
class FindElement {
    browser;
    log;
    constructor(browser, log) {
        this.browser = browser;
        this.log = log;
    }
    findElement(selector, type, shadow) {
        // console.log('type.toUpperCase()',type.toUpperCase())
        try {
            if (selector && type) {
                switch (type.toUpperCase()) {
                    case 'ID':
                        return this.findElementID(selector, shadow);
                    case 'CSS':
                        return this.findElementCss(selector, shadow);
                    case 'XPATH':
                        return this.findElementXPATH(selector, shadow);
                    case 'CLASS':
                        return this.findElementClass(selector, shadow);
                    default:
                        return this.findElementID(selector, shadow);
                }
            }
            else {
                this.log.loggererror("CLASS FINDELEMENT - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
                throw new Error("CLASS FINDELEMENT - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        }
        catch (error) {
            this.log.loggererror("CLASS FINDELEMENT - NO PUDO ENCONTRAR EL ELEMENTO:" + error);
            throw new Error("Se genero un error throw find Element: " + error);
        }
    }
    findElementCss(selector, shadow) {
        const driver = this.browser.getDriver() || shadow;
        return driver.findElement(selenium_webdriver_1.By.css(selector));
    }
    findElementID(selector, shadow) {
        const driver = this.browser.getDriver() || shadow;
        return driver.findElement(selenium_webdriver_1.By.id(selector));
    }
    findElementXPATH(selector, shadow) {
        const driver = this.browser.getDriver() || shadow;
        // console.log('xpath', driver);
        return driver.findElement(selenium_webdriver_1.By.xpath(selector));
    }
    findElementClass(selector, shadow) {
        const driver = this.browser.getDriver() || shadow;
        return driver.findElement(selenium_webdriver_1.By.className(selector));
    }
}
exports.FindElement = FindElement;
