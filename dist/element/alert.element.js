"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Alerts = void 0;
class Alerts {
    browser;
    log;
    constructor(browser, log) {
        this.browser = browser;
        this.log = log;
    }
    async goToAlert() {
        try {
            this.log.loggerinfo("SE INGRESÓ AL ALERT");
            return this.browser.getDriver().switchTo().alert();
        }
        catch (error) {
            this.log.loggererror("CLASS ALERTS - METODO GOTOALERTS - THROW: " + error);
            throw new Error("Se genero un error throw goToAlert: " + error);
        }
    }
    async alertAcep() {
        try {
            const alert = await this.goToAlert();
            alert.accept();
            this.log.loggerinfo("SE ACEPTO EL ALERT");
        }
        catch (error) {
            this.log.loggererror("CLASS ALERTS - METODO ALERTACEP - THROW: " + error);
            throw new Error("Se genero un error throw alertAcep: " + error);
        }
    }
    async alertDismiss() {
        try {
            const alert = await this.goToAlert();
            alert.dismiss();
            this.log.loggerinfo("SE DESCARTO EL ALERT");
        }
        catch (error) {
            this.log.loggererror("CLASS ALERTS - METODO ALERTDISMISS - THROW: " + error);
            throw new Error("Se genero un error throw alertDismiss: " + error);
        }
    }
}
exports.Alerts = Alerts;
