import { Driver } from "selenium-webdriver/chrome";
import { Browser, Page } from "../lib";
import { Log4JS } from "../lib/log4js";
import { Action } from "./accion.interface.v2";
import { execAction } from "./acion.exec.v2";

interface Secuencia {
    secuencia: number,
    url: string,
    tittle: string,
    actions: Array<Action>,
    otherServer: boolean,
    otherServerID: string,
    sendOtherServer: boolean,
    close_navigate: boolean
}

export interface intExeSecuencia {
    page: Page,
    driver: Driver,
    browser: Browser,
    secuencia: Secuencia,
    log: Log4JS,
    source: any,
    sourceError?: Array<errorElement>
}

interface errorElement {
    name: string,
    actions: Array<Action>
}

export async function execSecuencia(eleSecuencia: intExeSecuencia): Promise<any> {
    // console.log('secuencia',page,driver,browser)
    // let secuenciasExec = [];
    let reIntentos = 0, lastIndexExec, index;
    if (eleSecuencia.secuencia.otherServer) {
        return eleSecuencia.secuencia;
    }

    eleSecuencia.page.setUrl(eleSecuencia.secuencia.url);
    await eleSecuencia.page.navigate()
    await eleSecuencia.driver.sleep(5000)

    eleSecuencia.log.loggerdebug("Longitud de secuencia: " + eleSecuencia.secuencia.actions.length)
    for (let i = 0; i < eleSecuencia.secuencia.actions.length; i++) {
        try {
            if (lastIndexExec == i) {
                reIntentos = reIntentos + 1;
            } else {
                reIntentos = 0
            }
            index = i;

            lastIndexExec = i;
            eleSecuencia.log.loggerinfo("EJECUTA SECUENCIA: " + i + "\t" + lastIndexExec)
            eleSecuencia.log.loggerinfo("NUMERO DE REINTENTO: " + reIntentos)

            if (eleSecuencia.secuencia.actions[i].index != i) {
                eleSecuencia.log.loggererror("(NTTDATA) EL INDEX DEL JSON NO CORRESPONDE, VERIFICA EL .JSON : INDEX ACTUAL: " + eleSecuencia.secuencia.actions[i].index + " INDEX CORRECTO:" + i)
                throw new Error("(NTTDATA) EL INDEX DEL JSON NO CORRESPONDE, VERIFICA EL .JSON : \n\n INDEX ACTUAL: " + eleSecuencia.secuencia.actions[i].index + "\n\nINDEX CORRECTO:" + i);
            }

            const rs = await execAction(eleSecuencia.browser, eleSecuencia.secuencia.actions[i], eleSecuencia.driver, eleSecuencia.page, eleSecuencia.log, eleSecuencia.source);

            if (eleSecuencia.secuencia.actions[i].action == "FIND-FORCE-ERROR" ) {
                const _error = eleSecuencia.secuencia.actions[i].parameters?.force_error;
                // eleSecuencia.log.loggerdebug("paso" + _error)
                if (!!_error?.source) {
                    // eleSecuencia.log.loggerdebug("paso1" + JSON.stringify(eleSecuencia.sourceError, null, 4))
                    const _eleError = eleSecuencia.sourceError?.find((a) => a.name == _error.nameSource);
                    // eleSecuencia.log.loggerdebug("paso" + JSON.stringify(_eleError, null, 4))
                    if (!!_eleError) {
                        // eleSecuencia.log.loggerdebug("inicio" )
                        for (let i = 0; i < _eleError?.actions.length; i++) {
                            const element = _eleError?.actions[i];
                            await execAction(eleSecuencia.browser, element, eleSecuencia.driver, eleSecuencia.page, eleSecuencia.log, eleSecuencia.source);
                        }
                        // eleSecuencia.log.loggerdebug("termino" )

                    } else {
                        eleSecuencia.log.loggererror("NO SE ENCONTRÓ EL NAME DENTRO DEL ARCHIVO ERROR.JSON")
                    }
                } else if (!!rs && rs != i) {
                    i = Number(rs - 1);
                }
            }
        } catch (err: any) {
            if (reIntentos >= 15) {
                const _screen: Action = {
                    action: "SCREEN-ALL-GENERATE",
                    parameters: {
                        nameImage: "screen_error.png"
                    }
                }
                await execAction(eleSecuencia.browser, _screen, eleSecuencia.driver, eleSecuencia.page, eleSecuencia.log, eleSecuencia.source);
                eleSecuencia.log.loggererror("(NTTDATA) SE SUPERO EL NUMERO DE REINTENTOS : \n\n Acción: " + JSON.stringify(eleSecuencia.secuencia.actions[i]) + "\n\nERROR:" + err)

                throw new Error("(NTTDATA) Se supero el número de REINTENTOS : \n\n Acción: " + JSON.stringify(eleSecuencia.secuencia.actions[i]) + "\n\nERROR:" + err);
            }

            if (eleSecuencia.secuencia.actions[i].action == "FIND-FORCE-ERROR") {
                eleSecuencia.log.loggerinfo("EL ELEMENTO BUSCADO NO EXISTE Y SE SEGUIRA CON LA SECUENCIA");
            } else {
                eleSecuencia.log.loggererror('(NTTDATA) ERROR GENERADO:' + JSON.stringify(err, null, 4))
                eleSecuencia.log.loggererror('(NTTDATA) REINTENTO DE SECUENCIA N°: ' + reIntentos)
                eleSecuencia.log.loggererror('(NTTDATA) REPITE SECUENCIA NUMERO: ' + i)
                i = i - 1
                await eleSecuencia.driver.sleep(5000);
            }
            // log.loggererror('## SECUENCIA-EXEC ACCION-ERROR-NEXT:' + JSON.stringify(secuencia.actions[i],null,4))
        }
    }
    // return arrRest;
    return;
}