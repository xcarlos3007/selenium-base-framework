"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.execAction = void 0;
const selenium_webdriver_1 = require("selenium-webdriver");
const element_1 = require("../element");
const element_2 = require("../element/element");
const screenshot_element_1 = require("../element/screenshot.element.");
const senKeys_accion_1 = require("../element/senKeys_accion");
// eslint-disable-next-line @typescript-eslint/no-unused-vars
async function execAction(browser, action, driver, page, log) {
    const _iframe = new element_1.Iframe(browser, log);
    try {
        const _find = new element_1.FindElement(browser, log);
        let _root;
        if (action.iframe) {
            _root = _find.findElement(action.iframe.element, action.iframe.type);
            // console.log("iframe",_root);
            await _iframe.setDriverIframe(_root);
        }
        let locator;
        switch (action.type) {
            case "css":
                locator = { css: action.element };
                break;
            case "id":
                locator = { id: action.element };
                break;
            case "xpath":
                locator = { xpath: action.element };
                break;
            default:
                locator = { id: action.element };
                break;
        }
        // console.log('locator',locator)
        if (action.timeoutElement) {
            const condition = selenium_webdriver_1.until.elementLocated(locator);
            await driver.wait(condition, action.timeoutElement);
        }
        console.log('accion', action);
        await _action(action, browser, log);
        if (action.iframe) {
            await _iframe.setDriverDefault();
            // console.log('exit iframe')
        }
        if (action["time-sleep-out"] && action.action.toUpperCase() != 'SLEEP' && action.action.toUpperCase() != 'WAIT') {
            await browser.sleep(action["time-sleep-out"]);
        }
        else if (action.action.toUpperCase() != 'SLEEP' && action.action.toUpperCase() != 'WAIT') {
            await browser.sleep(3000);
        }
        return;
    }
    catch (error) {
        if (action.iframe) {
            await _iframe.setDriverDefault();
            // console.log('exit iframe2')
            await browser.sleep(3000);
        }
        throw new Error("Se genero un error throw Accion Exec: " + error);
    }
}
exports.execAction = execAction;
async function _action(action, browser, log) {
    const _click = new element_1.ClickElement(browser, log);
    const _find = new element_1.FindElement(browser, log);
    const _elem = new element_2.Element(browser, log);
    const _screen = new screenshot_element_1.ScreenShotElement(browser, log);
    const _sendKeysAction = new senKeys_accion_1.SendKeysAccion(browser, log);
    let domShadowRoot;
    switch (action.action.toUpperCase()) {
        case 'FIND':
            await _find.findElement(action.element, action.type, domShadowRoot);
            break;
        case 'FILL':
            // eslint-disable-next-line no-case-declarations
            const _eFill = await _find.findElement(action.element, action.type, domShadowRoot);
            await _eFill.sendKeys(action.value);
            break;
        case 'CLICK':
            await _click.onClick(action.element, action.type);
            break;
        case 'MOVE-CLICK':
            await _click.onMoveClick(action.element, action.type);
            break;
        case 'WAIT':
            await browser.wait(_find.findElement(action.element, action.type, domShadowRoot), action.time);
            break;
        case 'SLEEP':
            await browser.sleep(action.time);
            break;
        case 'SCROLL':
            await _elem.Scroll(action.element, action.type);
            break;
        case 'SCREEN-FIND-ELEMENT':
            await _screen.ScreenFindEle(action.element, action.type);
            break;
        case 'COPY-SCREEN':
            await _screen.copyScreen();
            break;
        case 'SCREEN-FIND-ELEMENT-SAVE':
            await _screen.ScreenFindEleSave(action.element, action.type, action.saveID);
            break;
        case 'PASTE-SCREEN':
            await _screen.pasteScreen(action.element, action.type);
            break;
        case 'UPDATE-URL':
            await browser.getDriver().navigate().to(action.url);
            break;
        case 'RELOAD':
            await browser.getDriver().navigate().refresh();
            break;
        // case 'GENERATE-IMAGE':
        //     await _screen.generateImage(action.saveID)
        //     break;
        case 'ENTER':
            await _sendKeysAction.onEnter(action.element, action.type);
            break;
        case 'SCREEN-FIND-GENERATE':
            // eslint-disable-next-line no-case-declarations
            const _imgBs64 = await _screen.ScreenFindEle(action.element, action.type);
            await _screen.ScreenGenerate(_imgBs64, action.nameImage);
            break;
        case 'CUT-IMAGE':
            await _screen.cutImage(action.cut);
            break;
        case 'SCREEN-ALL-GENERATE':
            // eslint-disable-next-line no-case-declarations
            const _screenBase64 = await _screen.ScreenAll();
            await _screen.ScreenGenerate(_screenBase64, action.nameImage);
            break;
        case 'FIND-FORCE-ERROR':
            const _eleFind = await _find.findElement(action.element, action.type, domShadowRoot);
            if (_eleFind) {
                throw new Error(`\n\nSE FORZO AL ERROR: FIND-FORCE-ERROR. \n\n EXISTE EL ELEMENTO "${action.element}" Y NO DEBE DE EXISTIR PARA QUE SE PUEDA CONTINUAR CON EL MONITOREO`);
            }
            console.log("_eleFind", _eleFind);
            break;
        default:
            console.log('(NTTDATA) No existe acción a realizar: ', action.action.toUpperCase());
            break;
    }
    return;
}
