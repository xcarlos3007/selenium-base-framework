"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Element = void 0;
const _1 = require(".");
class Element {
    browser;
    log;
    find;
    constructor(browser, log) {
        this.browser = browser;
        this.log = log;
        this.find = new _1.FindElement(browser, log);
    }
    //Baja el scroll a la posición del elemento
    async Scroll(selector, type) {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                // console.log('_ele',_ele)
                const driver = this.browser.getDriver();
                await driver.executeScript("arguments[0].scrollIntoView();", _ele);
            }
            else {
                this.log.loggererror("CLASS ELEMENT - METODO SCROLL - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
                throw new Error("CLASS ELEMENT - METODO SCROLL - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        }
        catch (error) {
            this.log.loggererror("CLASS ELEMENT - METODO SCROLL - THROW:" + error);
            throw new Error("Se genero un error throw Scroll: " + error);
        }
    }
    //Se agrega un método clear.
    async onClearInput(selector, type) {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                await _ele.clear();
            }
            else {
                this.log.loggererror("CLASS ELEMENT - METODO ONCLEARINPUT - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
                throw new Error("CLASS ELEMENT - METODO ONCLEARINPUT - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        }
        catch (error) {
            this.log.loggererror("CLASS ELEMENT - METODO ONCLEARINPUT - THROW:" + error);
            throw new Error("Se genero un error throw ONCLEARINPUT: " + error);
        }
    }
}
exports.Element = Element;
