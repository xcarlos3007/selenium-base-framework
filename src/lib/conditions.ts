import { WebElement } from 'selenium-webdriver';
import { Browser, WebComponent } from './';

export type WaitCondition = (browser: Browser) => Promise<boolean>;

export function elementIsVisible(locator: () => WebComponent): WaitCondition {
  return async () => await locator().isDisplayed();
}

export function elementIsPresent(locator: () => WebElement): WaitCondition {
  return async () => await locator() !== undefined;
}