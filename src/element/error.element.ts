import { Driver } from "selenium-webdriver/chrome";
import { FindElement } from ".";
import { forceError } from "../exec/accion.interface.v2";
import { Browser } from "../lib";
import { Log4JS } from "../lib/log4js";

export class ErrorElement {
    private find: FindElement;

    public constructor(protected browser: Browser, protected log: Log4JS) {
        this.find = new FindElement(browser, log);
    }

    //Baja el scroll a la posición del elemento
    public async findForceError(selector: string | undefined, type: string | undefined, force_error: forceError): Promise<any> {
        try {
            const _eleFind = await this.find.findElement(selector, type);
            if (_eleFind && force_error?.generate_excepcion) {
                this.log.loggererror(`\n\nSE FORZO AL ERROR: FIND-FORCE-ERROR. \n\n EXISTE EL ELEMENTO "${selector}" Y NO DEBE DE EXISTIR PARA QUE SE PUEDA CONTINUAR CON EL MONITOREO`)
                throw new Error(`\n\nSE FORZO AL ERROR: FIND-FORCE-ERROR. \n\n EXISTE EL ELEMENTO "${selector}" Y NO DEBE DE EXISTIR PARA QUE SE PUEDA CONTINUAR CON EL MONITOREO`);
            } else if (_eleFind && force_error?.source) {
                this.log.loggerinfo(`SE EJECUTARÁ UNA SECUENCIA ALTERNA RELACIONADA AL ERROR Y LUEGO SEGUIRÁ CON EL FLUJO NORMAL`)
                // return force_error?.exeIndex;
            } else if (_eleFind && force_error?.exeIndex) {
                this.log.loggerinfo(`SE MOVIÓ AL INDEX: ${force_error?.exeIndex}`)
                return force_error?.exeIndex;
            }
        } catch (error) {
            this.log.loggererror("CLASS ELEMENT - METODO SCROLL - THROW:" + error)
            throw new Error("Se genero un error throw Scroll: " + error);
        }
    }
}