"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ensure = void 0;
const _1 = require("./");
class WebComponentEnsurer {
    component;
    constructor(component) {
        this.component = component;
    }
    async textIs(expected) {
        const text = await this.component.getText();
        if (expected.trim() !== text.trim()) {
            throw new Error(`Element ${this.component.selector} text is '${text}'. Expected value is '${expected}'`);
        }
    }
    async isVisible() {
        if (!await this.component.isDisplayed()) {
            throw new Error(`Element ${this.component.selector} is visible`);
        }
    }
    async isNotVisible() {
        if (await this.component.isDisplayed()) {
            throw new Error(`Element ${this.component.selector} is visible`);
        }
    }
}
class ButtonEnsurer extends WebComponentEnsurer {
    button;
    constructor(button) {
        super(button);
        this.button = button;
    }
    async isNotDisabled() {
        if (await this.button.isDisabled()) {
            throw new Error(`Button ${this.button.selector} is disabled`);
        }
    }
}
class TextInputEnsurer extends WebComponentEnsurer {
    constructor(element) {
        super(element);
    }
}
function ensure(component) {
    if (component instanceof _1.Button) {
        return new ButtonEnsurer(component);
    }
    else if (component instanceof _1.WebComponent) {
        return new WebComponentEnsurer(component);
    }
}
exports.ensure = ensure;
