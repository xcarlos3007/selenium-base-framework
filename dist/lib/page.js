"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Page = void 0;
const conditions_1 = require("./conditions");
class Page {
    browser;
    url;
    setUrl(url) {
        this.url = url;
    }
    async navigate() {
        await this.browser.navigate(this.url);
        // await this.browser.wait(this.loadCondition(ele));
    }
    loadCondition(ele) {
        return (0, conditions_1.elementIsPresent)(() => ele);
    }
    constructor(browser) {
        this.browser = browser;
    }
}
exports.Page = Page;
