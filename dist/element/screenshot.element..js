"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScreenShotElement = void 0;
const _1 = require(".");
const selenium_webdriver_1 = require("selenium-webdriver");
const fs_1 = __importDefault(require("fs"));
const path_1 = require("../lib/path");
const sharp = require("sharp");
const path = __importStar(require("path"));
class ScreenShotElement {
    browser;
    log;
    find;
    constructor(browser, log) {
        this.browser = browser;
        this.log = log;
        this.find = new _1.FindElement(browser, log);
    }
    //Screen a un elemento
    async ScreenFindEle(selector, type) {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                return await _ele.takeScreenshot();
            }
            else {
                this.log.loggererror("CLASS SCREENSHOT - METODO SCREENFINDELE - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
                throw new Error("CLASS ELEMENT - METODO SCREENFINDELE - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        }
        catch (error) {
            throw new Error("Se genero un error throw ScreenFindEle: " + error);
        }
    }
    async ScreenAll() {
        try {
            return await this.browser.getDriver().takeScreenshot();
        }
        catch (error) {
            throw new Error("Se genero un error throw ScreenAll: " + error);
        }
    }
    async ScreenGenerate(screenBase64, nameImage) {
        // const _ele = await this.find.findElement(selector, type);
        // const _screen = await _ele.takeScreenshot();
        // console.log('_screen',_screen)
        try {
            let _imgName = nameImage ? nameImage : "default.png";
            this.log.loggerinfo('PATH SE GENERO LA IMAGEN: ' + (0, path_1.getPath)() + "/static/download/" + _imgName);
            fs_1.default.writeFile((0, path_1.getPath)() + '/static/download/' + _imgName, screenBase64, { encoding: 'base64', flag: 'w' }, function (err) {
                if (err)
                    console.log("(NTT-DATA) Error al Generar Img : ", err);
                else {
                    console.log("NTT-DATA SE GENERO LA IMAGEN.");
                }
            });
        }
        catch (error) {
            throw new Error("Se genero un error throw ScreenGenerate: " + error);
        }
    }
    async ScreenFindEleSave(selector, type, saveID) {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                const _screen = await _ele.takeScreenshot();
                // console.log('_screen', _screen)
                const strJson = fs_1.default.readFileSync((0, path_1.getPath)() + '/static/data.json').toString();
                const objeto = JSON.parse(strJson);
                const obj = {
                    "id": saveID,
                    "data": _screen
                };
                objeto.data.push(obj);
                fs_1.default.writeFileSync((0, path_1.getPath)() + '/static/data.json', JSON.stringify(objeto));
            }
            else {
                this.log.loggererror("CLASS SCREENSHOT - METODO SCREENFINDELEMENTSAVE - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
                throw new Error("CLASS ELEMENT - METODO SCREENFINDELEMENTSAVE - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        }
        catch (error) {
            throw new Error("Se genero un error throw ScreenFindEleSave: " + error);
        }
    }
    async copyScreen() {
        // console.log('copyScreen-new')
        try {
            await this.browser.getDriver().executeScript(() => {
                const header = document.querySelector("#imagen-sel");
                // console.log("header",header);
                const imgURL = header?.getAttribute("src") || "";
                // console.log(imgURL);
                async function asignaIMG() {
                    const data = await fetch(imgURL);
                    const _blob = await data.blob();
                    const type = _blob.type;
                    const blob = {};
                    blob[type] = _blob;
                    console.log(navigator, window);
                    await window.navigator.clipboard.write([new ClipboardItem(blob)]);
                }
                setTimeout(asignaIMG, 1000);
            });
        }
        catch (error) {
            throw new Error("Se genero un error throw copyScreen: " + error);
        }
    }
    async pasteScreen(selector, type) {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                await _ele.click();
                await _ele.sendKeys(selenium_webdriver_1.Key.CONTROL, "v");
                return;
            }
            else {
                this.log.loggererror("CLASS SCREENSHOT - METODO PASTESCREEN - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
                throw new Error("CLASS ELEMENT - METODO PASTESCREEN - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        }
        catch (error) {
            throw new Error("Se genero un error throw pasteScreen: " + error);
        }
    }
    async generateImage(ID) {
        try {
            const strJson = fs_1.default.readFileSync((0, path_1.getPath)() + '/static/data.json').toString();
            const objeto = JSON.parse(strJson);
            // console.log('ID', ID)
            const _find = objeto.data.find((ele) => ele.id == ID);
            // console.log('_find', _find)
            if (!_find.id) {
                console.log("(NTT-DATA) No encuentra el ID - DATA");
            }
            fs_1.default.writeFile((0, path_1.getPath)() + '/static/download/out.png', _find.data, 'base64', function (err) {
                if (err)
                    console.log("(NTT-DATA) Error al Generar Img : ", err);
            });
        }
        catch (error) {
            throw new Error("Se genero un error throw generateImage: " + error);
        }
    }
    async cutImage(cutImg) {
        try {
            if (cutImg == undefined) {
                throw new Error("Se genero un error throw cutImage Undefined");
            }
            sharp.cache(false);
            // console.log('cutpath',getPath() + '/static/download/', cutImg.nameImgID ? cutImg.nameImgID : "defaul.png")
            const _path = path.resolve((0, path_1.getPath)() + '/static/download/', cutImg.nameImgID ? cutImg.nameImgID : "defaul.png");
            const imageSH = sharp(_path);
            const _imgMeta = await imageSH.metadata();
            const realWidth = _imgMeta.width || 0;
            const realHeight = _imgMeta.height || 0;
            const topeAlto = cutImg.up;
            const topeIzquierda = cutImg.left;
            const altura = realHeight - (cutImg.up + cutImg.down);
            const ancho = realWidth - (cutImg.left + cutImg.right);
            const _nameIDImage = cutImg.nameImgID?.substring(0, cutImg.nameImgID.length - 3);
            const _nameImg = cutImg.nameImg ? cutImg.nameImg : _nameIDImage + "-cut.png";
            const _pathCut = (0, path_1.getPath)() + "/static/download/" + _nameImg;
            await imageSH.extract({ left: topeIzquierda, top: topeAlto, width: ancho, height: altura }).toFile(_pathCut);
        }
        catch (error) {
            throw new Error("Se genero un error throw cutImage: " + error);
        }
    }
}
exports.ScreenShotElement = ScreenShotElement;
