"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorElement = void 0;
const _1 = require(".");
class ErrorElement {
    browser;
    log;
    find;
    constructor(browser, log) {
        this.browser = browser;
        this.log = log;
        this.find = new _1.FindElement(browser, log);
    }
    //Baja el scroll a la posición del elemento
    async findForceError(selector, type, force_error) {
        try {
            const _eleFind = await this.find.findElement(selector, type);
            if (_eleFind && force_error?.generate_excepcion) {
                this.log.loggererror(`\n\nSE FORZO AL ERROR: FIND-FORCE-ERROR. \n\n EXISTE EL ELEMENTO "${selector}" Y NO DEBE DE EXISTIR PARA QUE SE PUEDA CONTINUAR CON EL MONITOREO`);
                throw new Error(`\n\nSE FORZO AL ERROR: FIND-FORCE-ERROR. \n\n EXISTE EL ELEMENTO "${selector}" Y NO DEBE DE EXISTIR PARA QUE SE PUEDA CONTINUAR CON EL MONITOREO`);
            }
            else if (_eleFind && force_error?.source) {
                this.log.loggerinfo(`SE EJECUTARÁ UNA SECUENCIA ALTERNA RELACIONADA AL ERROR Y LUEGO SEGUIRÁ CON EL FLUJO NORMAL`);
                // return force_error?.exeIndex;
            }
            else if (_eleFind && force_error?.exeIndex) {
                this.log.loggerinfo(`SE MOVIÓ AL INDEX: ${force_error?.exeIndex}`);
                return force_error?.exeIndex;
            }
        }
        catch (error) {
            this.log.loggererror("CLASS ELEMENT - METODO SCROLL - THROW:" + error);
            throw new Error("Se genero un error throw Scroll: " + error);
        }
    }
}
exports.ErrorElement = ErrorElement;
