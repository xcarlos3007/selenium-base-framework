"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const _1 = require(".");
const log4js_1 = require("./lib/log4js");
const path_1 = require("./lib/path");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const shell = require('shelljs');
(async () => {
    const strJson = fs_1.default.readFileSync('./src/productinventory.json').toString();
    const objeto = JSON.parse(strJson);
    const strConfig = fs_1.default.readFileSync('./src/config.json').toString();
    const configJson = JSON.parse(strConfig);
    const strError = fs_1.default.readFileSync('./src/error.json').toString();
    const errorJson = JSON.parse(strError);
    const browser = new _1.Browser(objeto.navigate, configJson.serverHUB);
    const page = new _1.Page(browser);
    const file = configJson.pathInit;
    (0, path_1.initPath)(file);
    const log = new log4js_1.Log4JS(file);
    const driver = browser.getDriver();
    try {
        const arrWpp = [];
        for (let i = 0; i < objeto.secuencias.length; i++) {
            const eleSecuencia = {
                page,
                driver,
                browser,
                secuencia: objeto.secuencias[i],
                log,
                source: configJson,
                sourceError: errorJson
            };
            const res = await (0, _1.execSecuencia)(eleSecuencia);
            if (res) {
                arrWpp.push(res);
            }
        }
        // await execSecuencia(page, driver, browser, objeto.secuencias[2])
        //***SE CREA LA ESTRCTURA PARA WPP */
        // console.log('termino', arrWpp)
        const _eleWpp = {
            "proceso": configJson.proceso,
            "turno": "18",
            "mensajes": [
                {
                    "medio": configJson.wpp.medio,
                    "grupo": configJson.wpp.grupo,
                    "envios": Array()
                }
            ]
        };
        if (arrWpp.length) {
            for (let i = 0; i < arrWpp.length; i++) {
                const element = arrWpp[i];
                element.actions.forEach((ele) => {
                    const _env = {
                        "tipo": ele.type,
                        "mensaje": ele.value
                    };
                    _eleWpp.mensajes[0].envios.push(_env);
                });
            }
            fs_1.default.writeFileSync((0, path_1.getPath)() + '\\static\\data.json', JSON.stringify(_eleWpp));
        }
        /*END*/
        /*SE MUEVE LOS ARCHIVOS MULTIMEDIA A LA RUTA CONFIGURADA PARA SER OBTENIDO POR WPP */
        shell.cp(configJson.pathInit + "\\static\\download\\*", configJson.wpp["multimedia-path"]);
        /*END */
        if (objeto.close_navigate) {
            driver.close();
        }
    }
    catch (e) {
        driver.close();
        console.log('error index', e);
    }
})();
