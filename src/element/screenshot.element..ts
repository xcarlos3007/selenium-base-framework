import { FindElement } from ".";
import { Browser } from "../lib";
import { Key } from 'selenium-webdriver';
import fs from 'fs'
import { getPath } from '../lib/path';
import sharp = require('sharp')
import * as path from 'path'
import { get } from "http";
import { Log4JS } from "../lib/log4js";

export interface cutImg {
    up: number,
    down: number,
    left: number,
    right: number,
    nameImgID?: string,
    nameImg?: string
}

export class ScreenShotElement {
    private find: FindElement;

    public constructor(protected browser: Browser, protected log: Log4JS) {
        this.find = new FindElement(browser, log);
    }

    //Screen a un elemento
    public async ScreenFindEle(selector: string | undefined, type: string | undefined): Promise<any> {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                return await _ele.takeScreenshot();
            } else {
                this.log.loggererror("CLASS SCREENSHOT - METODO SCREENFINDELE - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type)
                throw new Error("CLASS ELEMENT - METODO SCREENFINDELE - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        } catch (error) {
            throw new Error("Se genero un error throw ScreenFindEle: " + error);
        }
    }

    public async ScreenAll(): Promise<any> {
        try {
            return await this.browser.getDriver().takeScreenshot();
        } catch (error) {
            throw new Error("Se genero un error throw ScreenAll: " + error);
        }
    }

    public async ScreenGenerate(screenBase64: string, nameImage?: string): Promise<any> {
        // const _ele = await this.find.findElement(selector, type);
        // const _screen = await _ele.takeScreenshot();
        // console.log('_screen',_screen)
        try {
            let _imgName = nameImage ? nameImage : "default.png"
            this.log.loggerinfo('PATH SE GENERO LA IMAGEN: ' + getPath() + "/static/download/" + _imgName)
            fs.writeFile(getPath() + '/static/download/' + _imgName, screenBase64, { encoding: 'base64', flag: 'w' }, function (err) {
                if (err)  console.log("(NTT-DATA) Error al Generar Img : ", err);
                else {
                    console.log("NTT-DATA SE GENERO LA IMAGEN.");
                }
            });
        } catch (error) {
            throw new Error("Se genero un error throw ScreenGenerate: " + error);
        }
    }

    public async ScreenFindEleSave(selector: string | undefined, type: string | undefined, saveID?: string): Promise<any> {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                const _screen = await _ele.takeScreenshot();
                // console.log('_screen', _screen)
                const strJson = fs.readFileSync(getPath() + '/static/data.json').toString();
                const objeto = JSON.parse(strJson);
                const obj = {
                    "id": saveID,
                    "data": _screen
                }
                objeto.data.push(obj)
                fs.writeFileSync(getPath() + '/static/data.json', JSON.stringify(objeto))
            } else {
                this.log.loggererror("CLASS SCREENSHOT - METODO SCREENFINDELEMENTSAVE - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type)
                throw new Error("CLASS ELEMENT - METODO SCREENFINDELEMENTSAVE - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        } catch (error) {
            throw new Error("Se genero un error throw ScreenFindEleSave: " + error);
        }
    }

    public async copyScreen(): Promise<any> {
        // console.log('copyScreen-new')
        try {
            await this.browser.getDriver().executeScript(() => {
                const header = document.querySelector("#imagen-sel");
                // console.log("header",header);
                const imgURL = header?.getAttribute("src") || "";
                // console.log(imgURL);
                async function asignaIMG(): Promise<void> {
                    const data = await fetch(imgURL);
                    const _blob = await data.blob();
                    const type = _blob.type;
                    const blob: Record<string, any> = {};
                    blob[type] = _blob;
                    console.log(navigator, window);
                    await window.navigator.clipboard.write([new ClipboardItem(blob)]);
                }
                setTimeout(asignaIMG, 1000);
            })
        } catch (error) {
            throw new Error("Se genero un error throw copyScreen: " + error);
        }

    }

    public async pasteScreen(selector: string | undefined, type: string | undefined): Promise<any> {
        try {
            if (selector && type) {
                const _ele = await this.find.findElement(selector, type);
                await _ele.click();
                await _ele.sendKeys(Key.CONTROL, "v");
                return;
            } else {
                this.log.loggererror("CLASS SCREENSHOT - METODO PASTESCREEN - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type)
                throw new Error("CLASS ELEMENT - METODO PASTESCREEN - PARAMETROS UNDEFINED: SELECTOR" + selector + "\t TYPE:" + type);
            }
        } catch (error) {
            throw new Error("Se genero un error throw pasteScreen: " + error);
        }

    }

    public async generateImage(ID: string): Promise<void> {
        try {
            const strJson = fs.readFileSync(getPath() + '/static/data.json').toString();
            const objeto = JSON.parse(strJson);
            // console.log('ID', ID)
            const _find = objeto.data.find((ele: Record<string, any>) => ele.id == ID)
            // console.log('_find', _find)
            if (!_find.id) { console.log("(NTT-DATA) No encuentra el ID - DATA"); }
            fs.writeFile(getPath() + '/static/download/out.png', _find.data, 'base64', function (err) {
                if (err)
                    console.log("(NTT-DATA) Error al Generar Img : ", err);
            });
        } catch (error) {
            throw new Error("Se genero un error throw generateImage: " + error);
        }

    }

    public async cutImage(cutImg: cutImg | undefined) {
        try {
            if (cutImg == undefined) {
                throw new Error("Se genero un error throw cutImage Undefined");
            }
            sharp.cache(false);
            // console.log('cutpath',getPath() + '/static/download/', cutImg.nameImgID ? cutImg.nameImgID : "defaul.png")
            const _path = path.resolve(getPath() + '/static/download/', cutImg.nameImgID ? cutImg.nameImgID : "defaul.png");
            const imageSH = sharp(_path);
            const _imgMeta = await imageSH.metadata()
            const realWidth = _imgMeta.width || 0;
            const realHeight = _imgMeta.height || 0;
            const topeAlto = cutImg.up;
            const topeIzquierda = cutImg.left;
            const altura = realHeight - (cutImg.up + cutImg.down);
            const ancho = realWidth - (cutImg.left + cutImg.right);
            const _nameIDImage = cutImg.nameImgID?.substring(0, cutImg.nameImgID.length - 3);
            const _nameImg = cutImg.nameImg ? cutImg.nameImg : _nameIDImage + "-cut.png";
            const _pathCut = getPath() + "/static/download/" + _nameImg;

            await imageSH.extract({ left: topeIzquierda, top: topeAlto, width: ancho, height: altura }).toFile(_pathCut);
        } catch (error) {
            throw new Error("Se genero un error throw cutImage: " + error);
        }

    }
}