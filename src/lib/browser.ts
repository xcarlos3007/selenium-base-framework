import 'chromedriver';
import 'edgedriver'
import { Builder, ThenableWebDriver, WebElementPromise, WebElementCondition, By } from 'selenium-webdriver';
import { WaitCondition } from './';


export class Browser {
  private driver: ThenableWebDriver;
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(browserName: string,server: string) {
    this.driver = new Builder().forBrowser(browserName)
      .usingServer(server)
      .build();
    this.driver.manage().window().maximize();
  }

  public async navigate(url: string): Promise<void> {
    await this.driver.navigate().to(url);
  }

  public getDriver(): ThenableWebDriver {
    return this.driver;
  }
  
  public async clearCookies(url?: string): Promise<void> {
    if (url) {
      const currentUrl = await this.driver.getCurrentUrl();
      await this.navigate(url);
      await this.driver.manage().deleteAllCookies();
      await this.navigate(currentUrl);
    } else {
      await this.driver.manage().deleteAllCookies();
    }
  }

  public async wait(locator: WebElementCondition | WebElementPromise, time: any = 5000): Promise<any> {
    await this.driver.wait(locator, time)
  }

  public async sleep(time: number | undefined): Promise<any> {
    await this.driver.sleep(time || 5000)
  }

  public async waitAny(conditions: WaitCondition | WaitCondition[]): Promise<void> {
    const all = (!(conditions instanceof Array)) ? [conditions] : conditions;

    await this.driver.wait(async () => {
      for (const condition of all) {
        try {
          if (await condition(this) === true) {
            return true;
          }
          continue;
        } catch (ex) {
          continue;
        }
      }
    });
  }

  public async close(): Promise<void> {
    await this.driver.close();
  }
}
